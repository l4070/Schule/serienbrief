# Kollaborative Dokumentenerstellung mit LaTeX

Workshop im Rahmen der INFOS2021 https://dl.gi.de/handle/20.500.12116/36970

Marei Peischl <marei@peitex.de> & André Hilbig <hilbig@uni-wuppertal.de>

## Serienbrief Demo

# Struktur dieses Verzeichnisses

- passwort-brief.md: Hauptkonfigurationsdatei mit Templateauswahl
- passwort-brief_content.md: Text für den Brief. Aus technischen Gründen ist hier eine Extra-Datei notwendig
- maxmuster.lco: Absenderadressdaten
- template-serienbrief.tex: TeX Konfigurationsdatei, die vorgibt, wie die Markdown Dateien eingebunden werden sollen

# Das Projekt lokal bauen

Vorraussetzung ist eine aktuelle TeX Live Installation mit latexmk (https://www.tug.org/texlive/acquire-netinstall.html) unter Windows ist ggf. die Installation von perl notwendig.

Anschließend genügt es im Basisverzeichnis `latexmk -r latex-markdown-templates/CI.rc` auszuführen